using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TAN_Generator_GUI
{
    public partial class tan_generator : Form
    {
        public tan_generator()
        {
            InitializeComponent();

            ToolTip help = new ToolTip();
            help.SetToolTip(txt_input, "TAN-Länge mit den Ziffern 6 oder 9 eingeben");
            help.SetToolTip(txt_output, "Erzeugte TAN");
            help.SetToolTip(cmd_generate, "TAN erzeugen");
            help.SetToolTip(cmd_clear, "Ausgabe löschen");
            help.SetToolTip(cmd_end, "Programm beenden");
        }

        private void cmd_end_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void cmd_clear_Click(object sender, EventArgs e)
        {
            txt_input.Text = txt_output.Text = "";
            txt_input.Focus();
        }

        private void cmd_generate_Click(object sender, EventArgs e)
        {
            try
            {
                txt_output.Clear();

                if (txt_input.Text == "6")
                {
                    int[] zahlen = new int[6];
                    Random zufall = new Random();
                    for (int i = 0; i < 6; i = i + 1)
                        zahlen[i] = zufall.Next(0, 10);
                    for (int i = 0; i < 6; i = i + 1)
                        txt_output.Text += zahlen[i].ToString();
                }
                else
                    if (txt_input.Text == "9")
                    {
                        int[] zahlen = new int[9];
                        Random zufall = new Random();
                        for (int i = 0; i < 9; i = i + 1)
                            zahlen[i] = zufall.Next(0, 10);
                        for (int i = 0; i < 9; i = i + 1)
                            txt_output.Text += zahlen[i].ToString();
                    }
                    else
                        MessageBox.Show("Falsche Eingabe! \n" +
                        "Es sind nur 6 oder 9-stellige TAN's erlaubt. \n", "Fehler", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch
            {
                MessageBox.Show("Bitte die Ziffern 6 oder 9 eingeben!");
                txt_input.Clear();
                txt_input.Focus();
            }
        }
    }
}