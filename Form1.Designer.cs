namespace TAN_Generator_GUI
{
    partial class tan_generator
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txt_input = new System.Windows.Forms.TextBox();
            this.txt_output = new System.Windows.Forms.TextBox();
            this.cmd_end = new System.Windows.Forms.Button();
            this.cmd_clear = new System.Windows.Forms.Button();
            this.cmd_generate = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(109, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "TAN-Länge:";
            // 
            // txt_input
            // 
            this.txt_input.Location = new System.Drawing.Point(196, 49);
            this.txt_input.Name = "txt_input";
            this.txt_input.Size = new System.Drawing.Size(23, 22);
            this.txt_input.TabIndex = 1;
            // 
            // txt_output
            // 
            this.txt_output.BackColor = System.Drawing.SystemColors.Control;
            this.txt_output.Location = new System.Drawing.Point(112, 165);
            this.txt_output.Name = "txt_output";
            this.txt_output.ReadOnly = true;
            this.txt_output.Size = new System.Drawing.Size(107, 22);
            this.txt_output.TabIndex = 2;
            // 
            // cmd_end
            // 
            this.cmd_end.Location = new System.Drawing.Point(196, 244);
            this.cmd_end.Name = "cmd_end";
            this.cmd_end.Size = new System.Drawing.Size(75, 23);
            this.cmd_end.TabIndex = 3;
            this.cmd_end.TabStop = false;
            this.cmd_end.Text = "&Beenden";
            this.cmd_end.UseVisualStyleBackColor = true;
            this.cmd_end.Click += new System.EventHandler(this.cmd_end_Click);
            // 
            // cmd_clear
            // 
            this.cmd_clear.Location = new System.Drawing.Point(56, 244);
            this.cmd_clear.Name = "cmd_clear";
            this.cmd_clear.Size = new System.Drawing.Size(75, 23);
            this.cmd_clear.TabIndex = 4;
            this.cmd_clear.TabStop = false;
            this.cmd_clear.Text = "&Löschen";
            this.cmd_clear.UseVisualStyleBackColor = true;
            this.cmd_clear.Click += new System.EventHandler(this.cmd_clear_Click);
            // 
            // cmd_generate
            // 
            this.cmd_generate.Location = new System.Drawing.Point(112, 98);
            this.cmd_generate.Name = "cmd_generate";
            this.cmd_generate.Size = new System.Drawing.Size(107, 34);
            this.cmd_generate.TabIndex = 2;
            this.cmd_generate.Text = "&Generieren";
            this.cmd_generate.UseVisualStyleBackColor = true;
            this.cmd_generate.Click += new System.EventHandler(this.cmd_generate_Click);
            // 
            // tan_generator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(345, 321);
            this.Controls.Add(this.cmd_generate);
            this.Controls.Add(this.cmd_clear);
            this.Controls.Add(this.cmd_end);
            this.Controls.Add(this.txt_output);
            this.Controls.Add(this.txt_input);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "tan_generator";
            this.Text = "TAN-Generator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_input;
        private System.Windows.Forms.TextBox txt_output;
        private System.Windows.Forms.Button cmd_end;
        private System.Windows.Forms.Button cmd_clear;
        private System.Windows.Forms.Button cmd_generate;
    }
}

